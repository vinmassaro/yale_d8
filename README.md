# Yale University

Replace this with a brief description of the Yale University project.

Documentation by role:

* Developer
    * [Onboarding](readme/onboarding.md): “how do I get up and running on project work?”
    * [Repository architecture](readme/repo-architecture.md): “how is the code organized, and why?”
    * [Running project tasks](readme/project-tasks.md): “how do I _____ on my local machine?”
    * [Best practices](readme/best-practices.md): "how should I write code?"
    * [Workflow](readme/dev-workflow.md): “how do I contribute my code to this project?”
    * [Automated testing](tests/README.md): “how do I write / run them, and why should I care?”
* Technical Architect
    * [Project Architecture document](readme/architecture.md)
    * [Deploying to cloud](readme/deploy.md)
    * [Release process](readme/release-process.md)
    * [Setting up continuous integration](build/README.md#ci)
    * [Open source contribution](readme/os-contribution.md)

## Resources

* JIRA - https://itsyale.atlassian.net/browse/YALEUNV
* Git Code Repository - https://gitlab.com/open-ivy/yale_d8.git
* Acquia Cloud subscription - https://insight.acquia.com/cloud/workflow?s=2666111
* Acquia Cloud DEV instance - http://yalesite6dev.prod.acquia-sites.com/
