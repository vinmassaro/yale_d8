<?php

namespace Drupal\yale_content_demo\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Drupal\paragraphs\Entity\Paragraph;
use Drupal\migrate\MigrateException;

/**
 * Migration process plugin for migrating a paragraph.
 *
 * @MigrateProcessPlugin(
 *   id = "paragraphs_import"
 * )
 */
class ParagraphsImport extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    if (!isset($this->configuration['paragraph_type'])) {
      throw new MigrateException('Specify a paragraph type.');
    }
    if (!isset($this->configuration['fields'])) {
      throw new MigrateException('Give fields in same order as source values.');
    }
    if ($value && is_array($value)) {
      $paragraph_values = [];
      foreach ($this->configuration['fields'] as $idx => $field) {
        foreach ($field as $field_name => $source_field) {
          // Only populate fields with values.
          if (isset($value[$source_field]) && !is_null($value[$source_field])) {
            // Extra handling for sub-elements in some forms like "link".
            if (strpos($field_name, '/') !== FALSE) {
              $field_name_array = explode('/', $field_name);
              $paragraph_values[$field_name_array[0]][$field_name_array[1]] = $value[$source_field];
            }
            elseif ($source_field == 'filename') {
              $source = drupal_get_path('module', 'yale_content_demo') . '/data/images/' . $value[$source_field];
              $uri = file_unmanaged_copy($source);
              if ($uri) {
                $file = \Drupal::entityTypeManager()
                  ->getStorage('file')
                  ->create(['uri' => $uri]);
                $file->save();
                $paragraph_values[$field_name]['target_id'] = $file->id();
                $paragraph_values[$field_name]['url'] = $file->url();
                $paragraph_values[$field_name]['target_type'] = 'file';
              }

            }
            else {
              $paragraph_values[$field_name]['value'] = $value[$source_field];
            }
          }
        }
      }
      // don't create empty paragraphs.
      if (count($paragraph_values)) {
        $paragraph_values = array_merge(
          [
            'id' => NULL,
            'type' => $this->configuration['paragraph_type'],
          ],
          $paragraph_values);
$paragraph = Paragraph::create($paragraph_values);
$paragraph->save();

return $paragraph;
      }
    }
  }

}
