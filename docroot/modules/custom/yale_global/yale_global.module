<?php

/**
 * @file
 * Functionality for the yale_global module.
 */

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\filter\Entity\FilterFormat;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\Role;
use Drupal\node\Entity\NodeType;
use Drupal\scheduled_updates\Entity\ScheduledUpdateType;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\BlockPluginInterface;

/**
 * Implements hook_modules_installed().
 *
 * Turn off workbench moderation on default lightning modules after
 * the profile has been installed.
 *
 * Remove extraneous roles created by lightning, and turn off the
 * config settings on the lightning config page.
 */
function yale_global_modules_installed($modules) {
  // Turn off workbench moderation on lightning content types.
  if (in_array('lightning_workflow', $modules)) {
    _yale_global_turn_off_workbench_moderation();
  }

  // Remove extended lightning roles.
  _yale_global_turn_off_lightning_roles();

  // Remove workflow scheduled update types.
  _yale_global_remove_schedule_update_types();

  // Remove lightning's Rich Text format in favour of our own.
  if (in_array('lightning_media', $modules)) {
    FilterFormat::load('rich_text')->disable()->save();
  }

  // Uninstall lightning_layout, delete it's landing_page content type.
  // We replace it with our own landing_page content type.
  if (in_array('lightning_layout', $modules)) {
    /** @var \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller */
    $moduleInstaller = \Drupal::service('module_installer');
    $moduleInstaller->uninstall([
      'lightning_layout',
    ]);
    NodeType::load('landing_page')->delete();
  }

  // Alter settings after profile is installed.
  if (in_array('lightning', $modules)) {

    // Update the default theme after lightning_install() sets it to Bartik.
    // The yale_foundation theme is already enabled in yale_global_install().
    \Drupal::configFactory()->getEditable('system.theme')
      ->set('default', 'yale_foundation')
      ->save();

    // Create a basic front page node.
    Node::create([
      'type' => 'page',
      'title' => 'Front Page',
      'path' => [
        'alias' => '/frontpage',
      ],
      'body' => [
        'summary' => '',
        'value' => '<p>Welcome to your new YaleSite. To help get you started, we have provided you with some sample content and navigation links. Visit the <a href="http://yalesites.yale.edu/book/getting-started" target="_blank">YaleSites How-To Guide</a> for a comprehensive set of instructions and useful tools as you begin building out your site. The How-To Guide provides a basic <a href="http://yalesites.yale.edu/book/key-concepts" target="_blank">understanding of your YaleSite</a>, including guidance on <a href="http://yalesites.yale.edu/book/working-content" target="_blank">creating pages and adding content</a>, building navigation menus, and <a href="http://yalesites.yale.edu/book/changing-look-feel" target="_blank">changing the look and feel</a>. Need to shield your site from public view while you work on it? <a href="http://yalesites.yale.edu/book/preventing-site-visitors-seeing-your-site" target="_blank">Take your site off-line</a> so only people you authorize can see it. Happy building!</p>',
        'format' => 'full_html',
      ],
      'status' => NODE_PUBLISHED,
    ])->save();

    // Update site information.
    // If site install is run via drush, even without options, some of these
    // values will be overridden (e.g. The site name will be "Site Install").
    \Drupal::configFactory()->getEditable('system.site')
      ->set('name', 'Website Name')
      ->set('slogan', 'Welcome')
      ->set('mail', 'noreply@yale.edu')
      ->set('page.front', '/frontpage')
      ->save();
  }
}

/**
 * Turns off workbench moderation on default lightning content types.
 */
function _yale_global_turn_off_workbench_moderation() {

  $node_types = array(
    'page',
    'landing_page',
  );

  foreach ($node_types as $type_name) {
    // Load the content type.
    $node_type = NodeType::load($type_name);

    if (!is_null($node_type)) {
      // Turn off workbench moderation.
      $node_type->setThirdPartySetting('workbench_moderation', 'enabled', FALSE);

      // Save the content type.
      $node_type->save();
    }
  }
}

/**
 * Helper function to turn off lighting's extended content roles.
 */
function _yale_global_turn_off_lightning_roles() {
  // Turn off the settings on the lightning config page that create
  // the extraneous roles.
  $roles = lightning_read_config('lightning_core.settings', 'lightning_core');
  if (isset($roles['content_roles'])) {
    // Turn off the config settings on the lighting config page.
    foreach ($roles['content_roles'] as $role_id => $role) {
      $roles['content_roles'][$role_id]['enabled'] = FALSE;
    }

    // Save the config.
    \Drupal::configFactory()
      ->getEditable('lightning_core.settings')
      ->setData($roles)
      ->save();

    // Now actually delete the roles.
    $content_roles = \Drupal::config('lightning_core.settings')->get('content_roles');
    $node_types = NodeType::loadMultiple();
    foreach ($node_types as $node_type) {
      foreach (array_keys($content_roles) as $key) {
        $role = Role::load($node_type->id() . '_' . $key);
        if ($role) {
          $role->delete();
        }
      }
    }
  }

  // Remove role defined by lightning layout and media modules.
  $roles = array(
    'layout_manager',
    'media_creator',
    'media_manager',
  );
  foreach ($roles as $role_id) {
    $role = Role::load($role_id);
    if ($role) {
      $role->delete();
    }
  }
}

/**
 * Helper function to remove scheduled update types.
 */
function _yale_global_remove_schedule_update_types() {
  if (\Drupal::moduleHandler()->moduleExists('scheduled_updates')) {
    // Load all scheduled update types.
    $types = ScheduledUpdateType::loadMultiple();

    foreach ($types as $type) {
      $type->delete();
    }
  }
}

/**
 * Implements hook_form_FORM_ID_alter() for search_form().
 */
function yale_global_form_search_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  $search  = &$form['basic'];
  $classes = $search['#attributes']['class'];

  // Remove non-Foundation inline classes.
  $search['#attributes']['class'] = array_diff($classes, ['container-inline']);
  $search['#attributes']['class'][] = 'input-group';

  // Add the wrapper classes as prefix/suffix,
  // because FAPI doesn't let us pass attributes to the wrapper.
  $search['keys']['#prefix'] = '<div class="input-group-field">';
  $search['keys']['#suffix'] = '</div>';

  $search['submit']['#prefix'] = '<div class="input-button">';
  $search['submit']['#suffix'] = '</div>';
}

/**
 * Implements hook_layout_alter().
 *
 * Consolidates layouts provided by Panels and Zurb base theme.
 */
function yale_global_layout_alter(&$layout_info) {
  $category_map = [
    'Foundation: 1' => 'One Column',
    'Foundation: 2' => 'Two Columns',
    'Foundation: 3' => 'Three Columns',
    'Foundation: 4' => 'Four Columns',
  ];

  $remove_map = [
    'Columns: 1' => 'Other',
    'Columns: 2' => 'Other',
    'Columns: 3' => 'Other',
    'Foundation: 6' => 'Six Columns',
  ];

  foreach ($layout_info as $key => $info) {
    $category = $info['category'];
    if ($category instanceof TranslatableMarkup) {
      $category = $category->getUntranslatedString();
    }
    if (isset($category_map[$category])) {
      $layout_info[$key]['label'] = str_replace('Foundation: ', '', ucwords($layout_info[$key]['label']));
      $layout_info[$key]['category'] = $category_map[$category];
    }
    if (isset($remove_map[$category])) {
      unset($layout_info[$key]);
    }
  }
}

/**
 * Implements hook_block_build_alter().
 */
function yale_global_block_build_alter(array &$build, BlockPluginInterface $block) {
  $plugin_id = $block->getPluginId();

  if (in_array($plugin_id, array('system_breadcrumb_block'))) {
    $renderer = \Drupal::service('renderer');
    $config = \Drupal::config('block.block.breadcrumbs');

    // Vary the breadcrumb by the 'url.path' context to prevent cached
    // breadcrumb markup from rendering in accurate breadcrumbs.
    $build['#cache']['contexts'] = [
      'url.path',
    ];

    // Merge the additional cache context into existing cache settings.
    $renderer->addCacheableDependency($build, $config);
  }
}
