<?php

$drush_major_version = 8;

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site for local drupalvm
$home = drush_server_home();
$aliases['vm'] = array(
  'root' => '/var/www/yale/docroot',
  'uri' => 'http://yale.dev',
  'remote-host' => 'yale.dev',
  'remote-user' => 'vagrant',
  'ssh-options' => "-o PasswordAuthentication=no -i $home/.vagrant.d/insecure_private_key",
);
// Site yalesite6, environment dev
$aliases['dev'] = array(
  'root' => '/var/www/html/yalesite6.dev/docroot',
  'ac-site' => 'yalesite6',
  'ac-env' => 'dev',
  'ac-realm' => 'prod',
  'uri' => 'yalesite6dev.prod.acquia-sites.com',
  'remote-host' => 'staging-14952.prod.hosting.acquia.com',
  'remote-user' => 'yalesite6.dev',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['dev.livedev'] = array(
  'parent' => '@yalesite6.dev',
  'root' => '/mnt/gfs/yalesite6.dev/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site yalesite6, environment prod
$aliases['prod'] = array(
  'root' => '/var/www/html/yalesite6.prod/docroot',
  'ac-site' => 'yalesite6',
  'ac-env' => 'prod',
  'ac-realm' => 'prod',
  'uri' => 'yalesite6.prod.acquia-sites.com',
  'remote-host' => 'ded-14950.prod.hosting.acquia.com',
  'remote-user' => 'yalesite6.prod',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['prod.livedev'] = array(
  'parent' => '@yalesite6.prod',
  'root' => '/mnt/gfs/yalesite6.prod/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site yalesite6, environment ra
$aliases['ra'] = array(
  'root' => '/var/www/html/yalesite6.ra/docroot',
  'ac-site' => 'yalesite6',
  'ac-env' => 'ra',
  'ac-realm' => 'prod',
  'uri' => 'yalesite6ra.prod.acquia-sites.com',
  'remote-host' => 'staging-14299.prod.hosting.acquia.com',
  'remote-user' => 'yalesite6.ra',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['ra.livedev'] = array(
  'parent' => '@yalesite6.ra',
  'root' => '/mnt/gfs/yalesite6.ra/livedev/docroot',
);

if (!isset($drush_major_version)) {
  $drush_version_components = explode('.', DRUSH_VERSION);
  $drush_major_version = $drush_version_components[0];
}
// Site yalesite6, environment test
$aliases['test'] = array(
  'root' => '/var/www/html/yalesite6.test/docroot',
  'ac-site' => 'yalesite6',
  'ac-env' => 'test',
  'ac-realm' => 'prod',
  'uri' => 'yalesite6stg.prod.acquia-sites.com',
  'remote-host' => 'staging-14952.prod.hosting.acquia.com',
  'remote-user' => 'yalesite6.test',
  'path-aliases' => array(
    '%drush-script' => 'drush' . $drush_major_version,
  )
);
$aliases['test.livedev'] = array(
  'parent' => '@yalesite6.test',
  'root' => '/mnt/gfs/yalesite6.test/livedev/docroot',
);
